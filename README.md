# <div align="center"> Course <div/>

## Account test:

- Email:
  -- admin@gmail.com
  -- instructor@gmail.com
  -- user@gmail.com

- Password: @CursusOJT123

- bimatsmall@gmail.com : bimatbig@12

-- Sort type:
{
0 : TopRate,
1 : ASCName,
2 : DESCName,
3 : Newest,
4 : Oldest
}
## 0. Fix style
- node_modules -> slick-carousel -> slick -> + slick.css -> add    overflow-x: clip in .slick-list
                                             + slick-theme.css -> add   color:  black !important; in .slick-prev:before,.slick-next:before
## 1. Setup and start project

- Press combination "CTRL + SHIFT + V" in Visual Studio Code (VS Code) to get a better visual.
- Open terminal and press "npm i" to install libraries, frameworkes and node_modules.
- Press "npm start" to run project.

## 2. Libs/frameworkes

- Libs:

  - React (): main library to build project(single page application).
  - Axios(): Fetch API.
  - Redux - Reduxtoolkit: Redux toolkit help to write Redux syntaxes esier, manage states.
  - React Hook Form.
  - Yup.
  - Fontawesomes.
  - Framer Motion

- Framework:
  - Tailwind CSS(CSS framework): style and responsesive.
