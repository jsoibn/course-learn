import axiosClient from "./axiosClient";

const courseApi = {
  // getAllCourses() {
  //   const url = "/Course";
  //   return axiosClient.get(url);
  // },

  //base on params, we can get courses by many ways: allcourses, top courses, ...
  getCourses(params) {
    const { offset, limit, minPrice, maxPrice, catalogIDs, courseSort } =
      params;

    let url = `/Course/filter?offset=${offset}&limit=${limit}&minPrice=${minPrice}&maxPrice=${maxPrice}&courseSort=${courseSort}`;

    if (!!catalogIDs) {
      url = `/Course/filter?offset=0&limit=4&minPrice=0&maxPrice=9999&catalogIDs=${catalogIDs}&courseSort=0`;
    }

    return axiosClient.get(url);
  },

  getCatalog() {
    const url = "/Catalog";
    return axiosClient.get(url);
  },
  getCoursesByIns(params) {
    const { offset, limit } = params;
    let url = `/Course/get-by-instructor?offset=${offset}&limit=${limit}`;
    return axiosClient.get(url);
  },
  create_course(params) {
    const url = "/Course/create";
    return axiosClient.post(url, params);
  },
  update_course(params) {
    const url = "/Course/update";
    return axiosClient.put(url, params);
  },
  delete_course(id) {
    const url = `/Course/delete?id=${id}`;
    return axiosClient.delete(url);
  },
};

export default courseApi;
