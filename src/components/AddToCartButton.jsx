import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addToCartS, deleteCartItem } from "../store/slices/cartSlice";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";

function AddToCartButton({ course }) {
  const dispatch = useDispatch();
  async function addToCartAsync(dispatch, course) {
    // Assume addToCartS returns a promise
    await dispatch(addToCartS(course));
  }

  async function deleteItemInCartAsync(dispatch, id) {
    // Assume addToCartS returns a promise
    await dispatch(deleteCartItem(id));
  }

  const [isInCart, setIsInCart] = useState(false);
  const { coursesInOrder, isLoading } = useSelector(
    (state) => state.cartReducer
  );
  const addToCart = async (course) => {
    await addToCartAsync(dispatch, course);
    // Check the updated state after the asynchronous operation
    setIsInCart(true);
  };

  const handleDeleteItemInCart = async (id) => {
    await deleteItemInCartAsync(dispatch, id);

    setIsInCart(false);
  };

  useEffect(() => {
    setIsInCart(coursesInOrder.some((item) => item.courseID === course.id));
  }, [coursesInOrder]);
  return (
    <>
      {!isInCart ? (
        <div
          onClick={() => addToCart(course)}
          className="cursor-pointer text-white flex items-center justify-center 
        bg-blue-600
       px-4 py-2 rounded-[5px]"
        >
          <p className="font-medium  text-[20px] pr-4 ">Add to cart</p>

          <FontAwesomeIcon
            icon={faShoppingCart}
            className="pr-[5%] text-[20px]"
          />
        </div>
      ) : (
        <div
          onClick={() => handleDeleteItemInCart(course.id)}
          className="cursor-pointer text-white flex items-center justify-center
        bg-red-600
      px-4 py-2 rounded-[5px]"
        >
          <p className="font-medium  text-[20px] pr-4 ">Already in Cart</p>

          <FontAwesomeIcon
            icon={faShoppingCart}
            className="pr-[5%] text-[20px]"
          />
        </div>
      )}
    </>
  );
}

export default AddToCartButton;
