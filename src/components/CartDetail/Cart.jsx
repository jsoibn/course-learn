import React, { useEffect } from "react";

import InfoCart from "./InfoCart";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { Rate } from "antd";
import { Link } from "react-router-dom";
import TopCourse from "../LandingPage/Features/TopCourse/TopCourse";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteCartItem,
  getCart,
  removeCartS,
} from "../../store/slices/cartSlice";
import cartApi from "../../api/cartApi";
import InstructorCart from "./InstructorCart";

export default function Cart() {
  //declare
  const dispatch = useDispatch();
  const { coursesInOrder, totalQuantity } = useSelector(
    (state) => state.cartReducer
  );
  const totalPrice = coursesInOrder.reduce((currentPrice, course) => {
    return currentPrice + course.price;
  }, 0);
  const handleDelete = (course, e) => {
    e.preventDefault();
    dispatch(deleteCartItem(course.courseID));
  };
  useEffect(() => {
    dispatch(getCart());
  }, [dispatch]);
  return (
    <div className="laptop:px-[10%] desktop:px-[20%] py-[3%] ">
      <h1 className="font-bold text-[40px] pb-[2%]">Your Cart</h1>
      <div className="grid grid-cols-10 gap-[5%] pb-[10%]">
        <div className="col-span-7">
          <p className="font-bold text-[16px]">
            {totalQuantity} course in cart
          </p>
          {coursesInOrder &&
            coursesInOrder.map((course) => (
              <Link to={`/courses/course-detail/${course.courseID}`}>
                {" "}
                <div className=" grid grid-cols-10 gap-[2%] my-[2%] py-[2%] border-t border-solid border-gray_2">
                  <img
                    src={course.image}
                    alt="404"
                    className="w-full h-[100px] col-span-2"
                  />
                  <div className="flex flex-col col-span-6">
                    <h1 className="font-bold text-[20px] ">
                      {course.courseName}
                    </h1>

                    <p className="text-[16px] text-[#4c4c4c]">
                      <InstructorCart instructorID={course.instructorID} />
                    </p>
                    <p className="text-[#4a6412] font-normal py-[2%] ">
                      <FontAwesomeIcon className="pr-2" icon={faCheck} />
                      Total time{" "}
                      <FontAwesomeIcon className="px-2" icon={faCheck} />
                      Number Section
                    </p>
                    <p>{new Date(course.createDate).toLocaleString()}</p>
                  </div>

                  <div className="flex flex-col flex-grow items-end justify-between col-span-2">
                    <FontAwesomeIcon
                      onClick={(e) => handleDelete(course, e)}
                      className="pr-2 z-10 text-red-500 "
                      icon={faTrash}
                    />
                    <h1 className="text-[25px] text-[#15579d] font-bold">
                      {" "}
                      {course.price.toFixed(2)} $
                    </h1>
                  </div>
                </div>
              </Link>
            ))}
        </div>

        <div className="col-span-3">
          <InfoCart totalPrice={totalPrice} />
        </div>
      </div>
      {/* <TopCourse /> */}
    </div>
  );
}
