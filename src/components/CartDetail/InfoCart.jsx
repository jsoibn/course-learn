import React from 'react'

export default function InfoCart({totalPrice}) {
  return (
    <div className='flex flex-col'>
      <h1 className='font-bold text-[16px] pb-[5%] border-b border-solid border-gray_2'>Order Info</h1>
      <div className='flex justify-between py-2'>
      <h1 className='text-[#414141d4] font-medium text-[25px]'>Total</h1>
      <h1 className='text-[25px] text-[#15579d] font-bold'>{totalPrice} $</h1>
      </div>
      <button className='bg-blue_5 text-white p-2 text-[20px] font-medium rounded-[5px]' >Submit</button>
    </div>
  )
}
