import React, { useEffect, useState } from "react";

import { useSelector } from "react-redux";
import {
  getCoursesNew,
  getCoursesRate,
} from "../../../store/slices/courseSlice/courseSlice";
import CourseCard from "../../LandingPage/Features/TopCourse/CourseCard";

import Slider from "react-slick";
import ReusableCard from "./ReusableCard";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
export default function CoursesSuggest() {
  //const
  const { courses, listCoursesNew, listCoursesRate, listCoursesPoplar } =
    useSelector((state) => state.courseReducer);
  const [activeTab, setActiveTab] = useState("MostPopular");
  const switchTab = (tab) => {
    setActiveTab(tab);
  };
  //const Slider
  function Arrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      />
    );
  }
  const settings = {
    nextArrow: <Arrow />,
    prevArrow: <Arrow />,
    className: "center",
    infinite: false,
    centerPadding: "60px",
    slidesToShow: 5,
    swipeToSlide: true,
    afterChange: function (index) {
      console.log(
        `Slider Changed to: ${index + 1}, background: #222; color: #bada55`
      );
    },
  };
  return (
    <div>
      <ul className="flex gap-8 font-semibold pb-4 border-b border-solid border-gray_2">
        <li
          onClick={() => switchTab("MostPopular")}
          className={
            activeTab === "MostPopular" ? "" : "cursor-pointer text-[#0000007a]"
          }
        >
          Most Popular
        </li>
        <li
          onClick={() => switchTab("New")}
          className={
            activeTab === "New" ? "active" : "cursor-pointer  text-[#0000007a]"
          }
        >
          New
        </li>
        <li
          onClick={() => switchTab("TopRate")}
          className={
            activeTab === "TopRate"
              ? "active"
              : "cursor-pointer  text-[#0000007a]"
          }
        >
          Top Rate
        </li>
      </ul>
      {activeTab === "MostPopular" && (
        <>
          <Slider {...settings} className="pt-4">
            {listCoursesPoplar &&
              listCoursesPoplar.map((item) => (
                <ReusableCard key={item.id} course={item} />
              ))}
          </Slider>
        </>
      )}
      {activeTab === "New" && (
        <>
          <Slider {...settings} className="pt-4">
            {listCoursesNew &&
              listCoursesNew.map((item) => (
                <ReusableCard key={item.id} course={item} />
              ))}
          </Slider>
        </>
      )}
      {activeTab === "TopRate" && (
        <>
          <Slider {...settings} className="pt-4">
            {courses &&
              courses.map((item) => (
                <ReusableCard key={item.id} course={item} />
              ))}
          </Slider>
        </>
      )}
    </div>
  );
}
