import React from 'react'

export default function ReusableTitle({text}) {
  return (
    <>
        <h2 className='font-semibold text-[25px] pb-[20px]'>{text}</h2>
    </>
  )
}
