import React, { useState } from "react";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import SmallContent from "./SmallContent";
import { Rate } from "antd";
export default function ContentDetail({ course }) {
  const [showForm, setShowForm] = useState(1);
  const handleButtonClick = (value) => {
    setShowForm(value);
  };

  return (
    <>
      <div className="flex justify-around text-black border-gray_2 border-b border-solid pt-8 pb-4">
        <button
          onClick={() => handleButtonClick(1)}
          className="focus:text-blue-500"
        >
          About
        </button>
        <button
          onClick={() => handleButtonClick(2)}
          className="focus:text-blue-500"
        >
          Content
        </button>
        <button
          onClick={() => handleButtonClick(3)}
          className="focus:text-blue-500"
        >
          Instructor
        </button>
        <button
          onClick={() => handleButtonClick(4)}
          className="focus:text-blue-500"
        >
          Reviews
        </button>
      </div>

      {showForm === 1 && (
        <>
          <div className="w-full  border border-solid border-[#d6d6d6] rounded-[15px] h-200 bg-gray-100 p-4 my-8">
            <h1 className="font-bold text-[25px]">Course Overview</h1>
            <p className="text-[14px] text-gray-500">
              {course?.description || ""}
            </p>
          </div>
          <img
            className="w-full rounded-[15px]"
            src="/assests/images/course_detail/img_course.jpg"
            alt="img_course"
          ></img>
        </>
      )}

      {showForm === 2 && <SmallContent />}

      {showForm === 3 && (
        <>
          <div className="bg-gray-100 mt-8 p-7 rounded-[10px]">
            <div className="relative flex  border-solid border-b- pb-4 border-gray-300 ">
              <div>
                <img
                  src="/assests/images/avatar_tempt/avatar.jpg"
                  alt="img"
                  className="w-[100px] rounded-[50%]"
                ></img>
              </div>
              <div className="py-4 px-7">
                <h1 className="font-bold text-[20px]">Instructor Name</h1>
                <h2>specialization</h2>
              </div>

              <div className=" absolute right-0 bottom-[50%] translate-y-[-50%] text-yellow-400 font-bold flex">
                <Rate defaultValue={2} disabled></Rate>
              </div>
            </div>
            <p className="pt-6">
              Bio Instructor Bio Instructor Bio Instructor Bio Instructor Bio
              Instructor Bio Instructor Bio Instructor Bio Instructor Bio
              Instructor Bio Instructor Bio Instructor Bio Instructor Bio
              Instructor Bio Instructor Bio Instructor Bio Instructor Bio
              Instructor Bio Instructor Bio Instructor Bio Instructor Bio
              Instructor Bio Instructor Bio Instructor Bio Instructor
            </p>
          </div>
        </>
      )}
      {showForm === 4 && (
        <div className="bg-gray-100 mt-8 py-5 px-8 rounded-[10px] ">
          <div className="relative flex  border-solid border-b pb-4 border-gray-300 ">
            <img
              src="/assests/images/avatar_tempt/avatar.jpg"
              alt="img"
              className="w-[80px] h-[80px] rounded-[50%]"
            />

            <div className="px-7 ">
              <h1 className="font-bold text-[20px]">Review Name</h1>
              <Rate disabled value={4} />
              <h2>Date</h2>
            </div>
          </div>
          <p className="pt-2">
            Bio Instructor Bio Instructor Bio Instructor Bio Instructor Bio
            Instructor Bio Instructor Bio Instructor Bio Instructor Bio
            Instructor Bio Instructor Bio Instructor Bio Instructor Bio
            Instructor Bio Instructor Bio Instructor Bio Instructor Bio
            Instructor Bio Instructor Bio Instructor Bio Instructor Bio
            Instructor Bio Instructor Bio Instructor Bio Instructor
          </p>
        </div>
      )}
    </>
  );
}
