import { Rate } from "antd";
import React, { useState } from "react";
import { useSelector } from "react-redux";

export default function HeaderCourseDetail({ course }) {
  const { catalog } = useSelector((state) => state.courseReducer);
  const courseCatalogs = course?.catalogIDs;

  const catalogByCourse = catalog?.find((item) =>
    courseCatalogs?.includes(item.id)
  );

  return (
    <div className="col-span-10 bg-background_1 bg-background_gradient_1 relative w-full  h-[400px] ">
       <div className="absolute bottom-[10%] left-[6%] ">
          <h1 className="text-[30px] text-white font-semibold ">{course?.name || ""}</h1>
          <p className="text-white ">Instructor name</p>
          <div className="flex items-center">
           
            <p className=" mr-2 bg-[#78bd25] text-white rounded-[3px] text-center py px-4">
              {catalogByCourse?.name || ""}
            </p>
            <p className="text-[#fadb14] text-[16px] mr-2">
              {course?.avgRate || ""}
            </p>
            <Rate
              className="text-[16px]"
              allowHalf
              value={course?.avgRate || ""}
            />
          </div>
        </div>
      <img
        src="/assests/images/course_detail/img_course.jpg"
        className=" float-right  w-[50%] h-full"
        style={{    clipPath: 'polygon(60% -60%, 45% 0%, 10% 100%, 180% 100%)' }}
      />
    </div>
  );
}
