import React, { useState } from 'react';

import { faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
export default function SmallContent() {

    const [showDetail, setShowDetail] = useState(false)

    const handleShowDetail = () => {
        setShowDetail(!showDetail)
    }


    return (
        <div className='bg-gray-100 rounded-[10px] my-6 px-6 '>
            <div
                onClick={handleShowDetail}
                className='  py-4  border-b border-solid  border-gray_2'>
                <div className='flex justify-between items-center'>
                    <div className='mt-2'>
                        <h1 className='font-bold pb-1 text-2xl'>Name Section</h1>
                        <p>Section 1 - Total time</p>
                    </div>
                 
                        <FontAwesomeIcon className='text-[25px] text-blue_2' icon={faAngleUp} />
                    
                </div>
                {showDetail && (
                    <div className='mt-7'>
                        <div>Name Section</div>
                        <div className='mt-2'>Section description Section description Section description Section description  Section description Section description Section description Section description Section description  Section description Section description Section description Section description Section description  Section description Section description Section description Section description Section description  Section description </div>
                    </div>
                    
                )}
            </div>
        </div>


    )
}






