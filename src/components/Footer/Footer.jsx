import {
  faFacebook,
  faInstagram,
  faTwitter,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

function Footer() {
  return (
    <div className="bg-gray_9 px-10 pt-8 h-56 w-full text-white">
      <div className="flex justify-evenly items-start">
        <div className="w-1/3">
          <h3 className="font-bold mb-4">S Education Logo</h3>
          <p>
            S Education is an online education platform specializing in
            programming, offering high-quality courses to help you develop your
            skills in this field.
          </p>
        </div>
        <div className="w-1/3 flex flex-col items-center">
          <h3 className="font-bold mb-4">You can find us at</h3>
          <div className="flex ">
            <div className="w-10 h-10 flex justify-center items-center">
              <a href="https://www.facebook.com/">
                <FontAwesomeIcon icon={faFacebook} />
              </a>
            </div>
            <div className="w-10 h-10 flex justify-center items-center">
              <a href="https://www.instagram.com/">
                <FontAwesomeIcon icon={faInstagram} />
              </a>
            </div>
            <div className="w-10 h-10 flex justify-center items-center">
              <a href="https://twitter.com/">
                <FontAwesomeIcon icon={faTwitter} />
              </a>
            </div>
            <div className="w-10 h-10 flex justify-center items-center">
              <a href="https://www.youtube.com/">
                <FontAwesomeIcon icon={faYoutube} />
              </a>
            </div>
          </div>
        </div>
        <div className="w-1/3 h-40 bg-gray-200"></div>
      </div>
      <p></p>
    </div>
  );
}

export default Footer;
