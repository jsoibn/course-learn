import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Badge } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import ProfileDropdown from "./ProfileDropdown/ProfileDropdown";

function Header() {
  const { isLogin } = useSelector((state) => state.accountReducer);

  return (
    <div className="bg-transparent z-50 fixed top-0 left-0 w-full  ">
      <div className="flex justify-between py-1 px-8 items-center">
        <div className="w-32 h-20 rounded-full">
          <Link to="/">
            <img
              className="w-full rounded-full"
              src="/assests/images/Code_IT-removebg-preview.png"
              alt=""
              style={{ position: "relative", top: "-30%" }}
            />
          </Link>
        </div>
        <div>
          {isLogin ? (
            <ProfileDropdown />
          ) : (
            <Link to="/login">
              <button className="bg-black text-white py-2 px-4 rounded-3xl">
                Get started
              </button>
            </Link>
          )}
        </div>
      </div>
    </div>
  );
}

export default Header;
