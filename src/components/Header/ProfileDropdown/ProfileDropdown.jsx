import React, { useState, useEffect } from "react";

import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import storageService from "../../../api/storageService";
import { setIsLogin, setRole } from "../../../store/slices/accountSlice";
import { fetchUserProfile } from "../../../store/slices/userSlice";
function ProfileDropdown() {
  const [isDropdown, setIsDropdown] = useState(false);
  const [imageSrc, setImageSrc] = useState("assests/images/user-blank.png");
  const { role } = useSelector((state) => state.accountReducer);
  const dispatch = useDispatch();
  const { profile } = useSelector((state) => state.userReducer);
  useEffect(() => {
    dispatch(fetchUserProfile());
  }, [dispatch]);

  const navigate = useNavigate();
  const handleDropdown = () => {
    setIsDropdown(!isDropdown);
  };

  const handleLogout = () => {
    storageService.removeAccessToken();
    dispatch(setIsLogin(false));
    dispatch(setRole(""));
    storageService.removeRole();
    navigate("/");
    window.location.reload();
  };

  return (
    <>
      <div
        onClick={handleDropdown}
        className="w-12 h-12 bg-black rounded-full relative cursor-pointer"
      >
        <img
          className="rounded-full w-full h-full"
          src={profile ? profile.image : imageSrc}
          alt="Avatar"
        />
        {isDropdown && (
          <div className="z-10 absolute -bottom-20 right-0 mt-1 bg-white divide-y divide-gray-100 rounded-lg shadow w-24 ">
            <ul
              className="py-2 text-sm text-gray-700 "
              aria-labelledby="dropdownDefaultButton"
            >
              <li className="block text-center hover:text-blue-500">
                <Link to="/profile">Profile</Link>
              </li>

              <li className="h-full">
                <button
                  onClick={handleLogout}
                  className="block px-4 py-2 hover:text-blue-500 w-full "
                >
                  Sign out
                </button>
              </li>
            </ul>
          </div>
        )}
      </div>
    </>
  );
}

export default ProfileDropdown;
