
import React from "react";
import { Rate } from 'antd';
function ListCoursesCard() {
  return (
    <div className="bg-gray_5 p-4 my-7  flex  gap-4 shadow-box_shadow_2 rounded-[15px] ">
           <img
            src="/assests/images/landing_pages/land_page_2.jpg"
            className="w-[150px] h-[110px] rounded-[15px]"
            alt="img"
          />
          <div className="flex flex-col" >
            <h1 className="font-bold text-[20px]">Course Name</h1>
            <p className="line-clamp-2  text-text_color_2 text-[15px] ">  Course description Course description  Course description Course description  Course description Course description</p>
            <div className="flex justify-between items-center pt-2">
            <Rate className="text-[15px]" disabled defaultValue={2} />
            <p className="bg-blue_5 px-2 justify-center  flex  w-[70px] text-white rounded-full">Title</p>
            </div>
          </div>
      
    </div>
  );
}

export default ListCoursesCard;
