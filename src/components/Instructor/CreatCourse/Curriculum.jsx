import React, { useEffect, useState } from "react";
import { Button, Input, Modal, Form, Upload, Radio } from "antd";
import { Editor } from "@tinymce/tinymce-react";
import {
  PlusCircleOutlined,
  DeleteOutlined,
  UploadOutlined,
  VideoCameraOutlined,
  EditOutlined,
} from "@ant-design/icons";

const Curriculum = ({ current }) => {
  const [sections, setSections] = useState([]);
  const [isAddSectionModalVisible, setAddSectionModalVisible] = useState(false);
  const [isEditSectionModalVisible, setEditSectionModalVisible] =
    useState(false);
  const [isAddLessonModalVisible, setAddLessonModalVisible] = useState(false);
  const [fileList, setFileList] = useState([]);
  const [videoType, setVideoType] = useState("file");
  const [currentLesson, setCurrentLesson] = useState(null);
  const [lessonDescription, setLessonDescription] = useState("");
  const [form] = Form.useForm();
  const [selectedSectionIndex, setSelectedSectionIndex] = useState(null);

  const customRequest = ({ file, onSuccess, onError }) => {
    setTimeout(() => {
      onSuccess("ok");
    }, 1000);
  };

  const handleChange = (info) => {
    if (info.fileList.length > 1) {
      info.fileList.shift();
    }
    setFileList(info.fileList);
  };

  const showAddSectionModal = () => {
    setAddSectionModalVisible(true);
  };

  const handleAddSection = (values) => {
    const newSections = [...sections];
    newSections.push({
      name: values.sectionName,
      lessons: [],
    });

    setSections(newSections);

    setAddSectionModalVisible(false);
    form.resetFields();
  };

  const showEditSectionModal = () => {
    setEditSectionModalVisible(true);
  };

  const handleEditSection = (values) => {
    if (selectedSectionIndex !== null) {
      const newSections = [...sections];
      newSections[selectedSectionIndex].name = values.sectionName;
      setSections(newSections);
      setEditSectionModalVisible(false);
      form.resetFields();
    }
  };

  const deleteSection = () => {
    if (selectedSectionIndex !== null) {
      const newSections = [...sections];
      newSections.splice(selectedSectionIndex, 1);
      setSections(newSections);
      setEditSectionModalVisible(false);
      setSelectedSectionIndex(null);
    }
  };

  const showAddLessonModal = () => {
    setAddLessonModalVisible(true);
  };

  const handleAddLesson = (values) => {
    if (selectedSectionIndex !== null) {
      const newSections = [...sections];
      const lesson = {
        name: values.lessonName,
        description: lessonDescription,
        videoType: values.videoType,
        videoFile: values.videoType === "file" ? values.videoFile : "",
        videoURL: values.videoType === "url" ? values.videoURL : "",
      };
      newSections[selectedSectionIndex].lessons.push(lesson);
      setSections(newSections);
      setAddLessonModalVisible(false);
      setCurrentLesson(lesson);
      setSelectedSectionIndex(null);
      form.resetFields();
    }
  };

  return (
    <div>
      <Button onClick={showAddSectionModal}>
        <PlusCircleOutlined /> Add Section
      </Button>

      <Modal
        title="Add Section"
        visible={isAddSectionModalVisible}
        onOk={form.submit}
        onCancel={() => setAddSectionModalVisible(false)}
      >
        <Form form={form} layout="vertical" onFinish={handleAddSection}>
          <Form.Item name="sectionName" label="Name Section">
            <Input />
          </Form.Item>
        </Form>
      </Modal>

      {sections.map((section, sectionIndex) => (
        <div key={sectionIndex}>
          <h2 className="font-bold float-left mr-4">{section.name}</h2>
          <Button
            onClick={() => {
              setSelectedSectionIndex(sectionIndex);
              showEditSectionModal();
            }}
          >
            <EditOutlined />
          </Button>
          <Button className=" justify-end" onClick={deleteSection}>
            <DeleteOutlined />
          </Button>
          <Button
            onClick={() => {
              setSelectedSectionIndex(sectionIndex);
              showAddLessonModal();
            }}
          >
            <PlusCircleOutlined />
            Add Lesson
          </Button>
          {section.lessons.map((lesson, lessonIndex) => (
            <div key={lessonIndex}>
              <h3>Lesson: {lesson.name}</h3>
            </div>
          ))}
        </div>
      ))}

      <Modal
        title="Edit Section"
        visible={isEditSectionModalVisible}
        onOk={form.submit}
        onCancel={() => setEditSectionModalVisible(false)}
      >
        <Form form={form} onFinish={handleEditSection}>
          <Form.Item name="sectionName" label="Name Section">
            <Input />
          </Form.Item>
        </Form>
      </Modal>

      {selectedSectionIndex !== null && (
        <Modal
          title="Add Lesson"
          visible={isAddLessonModalVisible}
          onOk={form.submit}
          onCancel={() => setAddLessonModalVisible(false)}
        >
          <Form form={form} layout="vertical" onFinish={handleAddLesson}>
            <Form.Item name="lessonName" label="Name Lesson">
              <Input />
            </Form.Item>
            <Form.Item name="lessonDescription" label="Description Lesson">
              <Editor
                apiKey="ixvxozqrdtgx792bgfu9scv37l18iipbaf07nzzdsvjt17qs"
                onEditorChange={(content, editor) =>
                  setLessonDescription(content)
                }
              />
            </Form.Item>
            <Form.Item
              label="Video Type"
              name="videoType"
              rules={[{ required: true, message: "Please select video type" }]}
            >
              <Radio.Group onChange={(e) => setVideoType(e.target.value)}>
                <Radio.Button value="file">
                  <UploadOutlined /> Upload video by file
                </Radio.Button>
                <Radio.Button value="url">
                  <VideoCameraOutlined /> Upload video by URL
                </Radio.Button>
              </Radio.Group>
            </Form.Item>
            {videoType === "file" && (
              <Form.Item
                label="Upload Video File"
                name="videoFile"
                valuePropName="fileList"
                getValueFromEvent={(e) => e.fileList}
              >
                <Upload
                  customRequest={customRequest}
                  fileList={fileList}
                  onChange={handleChange}
                  beforeUpload={() => false}
                  maxCount={1}
                  listType="text"
                >
                  <Button icon={<UploadOutlined />}>Upload File</Button>
                </Upload>
              </Form.Item>
            )}
            {videoType === "url" && (
              <Form.Item
                label="Upload Video URL"
                name="videoURL"
                rules={[{ required: true, message: "Please enter video URL" }]}
              >
                <Input placeholder="Video URL" />
              </Form.Item>
            )}
          </Form>
        </Modal>
      )}
    </div>
  );
};

export default Curriculum;
