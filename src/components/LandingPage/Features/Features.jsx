import React from "react";
import TopCourse from "./TopCourse/TopCourse";
import TopInstructor from "./TopInstructor/TopInstructor";

function Features() {
  return (
    <div className="py-8 px-[10%]">
      <TopCourse />
      <TopInstructor />
    </div>
  );
}

export default Features;
