import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { Rate } from "antd";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addToCartS, deleteCartItem } from "../../../../store/slices/cartSlice";
import AddToCartButton from "../../../AddToCartButton";

function CourseCard({ isLastCard, course }) {
  const dispatch = useDispatch();
  const [isHovered, setIsHovered] = useState(false);

  const { coursesInOrder, isLoading } = useSelector(
    (state) => state.cartReducer
  );

  // setIsInCart(coursesInOrder.some((item) => item.courseID === course.id));
  const { catalog } = useSelector((state) => state.courseReducer);

  const courseCatalogs = course.catalogIDs;
  const catalogByCourse = catalog?.filter((item) =>
    courseCatalogs.includes(item.id)
  );

  const handleMouseHover = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  useEffect(() => {}, [course]);

  return (
    <div
      className={`group relative ${isHovered && "z-20"}`}
      onMouseEnter={handleMouseHover}
      onMouseLeave={handleMouseLeave}
    >
      <Link to={`/courses/course-detail/${course.id}`}>
        <div className="card-img w-full h-52">
          <img
            src={course.image}
            className="w-full h-full object-cover"
            alt="img"
          />
        </div>
        <div className="card-content bg-gray-100 w-full px-4 py-6">
          <div className="flex overflow-x-auto">
            {catalogByCourse &&
              catalogByCourse.map((item, index) => {
                return (
                  <p
                    key={index}
                    className="mb-2 mr-2 bg-blue-400 text-white rounded-md text-center py-1 px-4"
                  >
                    {item.name}
                  </p>
                );
              })}
          </div>

          {/* alo */}

          <h2 className="line-clamp-2 font-bold text-xl">{course.name}</h2>

          {/* loa */}

          <div className="flex items-center justify-between">
            <Rate
              className="text-[20px]"
              disabled
              allowHalf
              defaultValue={course.avgRate}
            />
            <p className="text-blue-400 font-bold">
              {course.price.toFixed(2)} $
            </p>
          </div>
        </div>
      </Link>
      <div
        className={` hidden	 group-hover:block ${
          isLastCard
            ? "group-hover:translate-x-[calc(-100%-1rem)]"
            : "group-hover:translate-x-[calc(100%+1rem)] "
        } z-50 p-4  absolute top-0 left-0 bg-black bg-opacity-100 rounded-[10px] `}
      >
        <h2 className="font-bold text-xl text-white pb-2">{course.name}</h2>
        <p className="font-semibold text-[13px] text-[#b9ff00]">
          <span className="text-[#88b85c]">Đã cập nhật</span> tháng 13 năm 2023
        </p>
        <p className="text-[#ffffffd9] text-[13px] pb-[10%]">
          Total time 10 hours
        </p>
        <ul className="text-[15px] text-white">
          <li className="pb-[5%] line-clamp-3">
            <FontAwesomeIcon icon={faCheck} className="pr-[5%] text-[20px]" />
            Outcome of course Outcome of course Outcome of course Outcome of
            course
          </li>
          <li className="pb-[5%] line-clamp-3">
            <FontAwesomeIcon icon={faCheck} className="pr-[5%] text-[20px]" />
            Outcome of course Outcome of course Outcome of course Outcome of
            course
          </li>
          <li className="pb-[5%] line-clamp-3">
            <FontAwesomeIcon icon={faCheck} className="pr-[5%] text-[20px]" />
            Outcome of course Outcome of course Outcome of course Outcome of
            course
          </li>
        </ul>
        <AddToCartButton course={course} />

        {isLastCard ? (
          <div className="absolute top-1/2 -right-4 w-8 h-8 bg-black transform rotate-45" />
        ) : (
          <div className="absolute top-1/2 -left-4 w-8 h-8 bg-black transform rotate-45" />
        )}
      </div>
    </div>
  );
}

export default CourseCard;
