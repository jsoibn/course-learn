import React from "react";
import CourseCard from "./CourseCard";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

function TopCourse() {
  const { courses, catalog } = useSelector((state) => state.courseReducer);

  return (
    <div className="mb-12">
      <h1
        className="font-bold text-3xl text-blue_3 text-center mb-2"
        data-aos="fade-down"
        data-aos-delay="200"
      >
        Our Top Courses
      </h1>
      <div className="flex justify-end">
        <Link to="/courses" className="text-blue_2 cursor-pointer mb-2">
          View all
        </Link>
      </div>
      <div className="w-full flex gap-4">
        {courses &&
          courses.slice(0, 4).map((item, index) => {
            return (
              <div className="w-1/4">
                <CourseCard
                  isLastCard={index === 3}
                  key={item?.id}
                  course={item}
                />
              </div>
            );
          })}
      </div>
    </div>
  );
}

export default TopCourse;
