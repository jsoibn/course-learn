import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

function InstructorCard() {
  return (
    <div className="w-full rounded-l-md flex h-[200px]">
      <div className="card-img rounded-md w-1/3 h-full">
        <img
          src="/assests/images/landing_pages/land_page_2.jpg"
          className="w-full h-full rounded-md"
          alt=""
        />
      </div>
      <div className="card-content bg-gray_1 w-2/3 h-full px-4 py-6 flex flex-col justify-evenly">
        <p className="mb-2 bg-blue-400 w-16 text-white rounded-md text-center py-1 px-4">
          Title
        </p>
        <div className="flex justify-between">
          <h2 className="mb-2 font-bold text-xl">Name Instructor</h2>
          <div className="flex justify-between">
            <div className="text-yellow-400 font-bold flex">
              <FontAwesomeIcon icon={faStar} />
              <FontAwesomeIcon icon={faStar} />
              <FontAwesomeIcon icon={faStar} />
              <FontAwesomeIcon icon={faStar} />
              <FontAwesomeIcon icon={faStar} />
            </div>
          </div>
        </div>
        <p className="">
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quibusdam n
        </p>
      </div>
    </div>
  );
}

export default InstructorCard;
