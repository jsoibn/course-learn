import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import InstructorCard from "./InstructorCard";

function ListInstructors() {
  return (
    <div className="flex w-full gap-6">
      <div className="w-1/2 h-[600px]">
        <div className="card-img w-full h-[400px]">
          <img
            src="/assests/images/landing_pages/land_page_2.jpg"
            className="w-full h-full"
            alt=""
          />
        </div>
        <div className="card-content bg-gray_1 w-full h-[200px] px-4 py-6 flex flex-col justify-evenly">
          <p className="mb-2 bg-blue-400 w-16 text-white rounded-md text-center py-1 px-4">
            Title
          </p>
          <div className="flex justify-between">
            <h2 className="mb-2 font-bold text-xl">Name Instructor</h2>
            <div className="flex justify-between">
              <div className="text-yellow-400 font-bold flex">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
              </div>
            </div>
          </div>
          <p className="">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quibusdam
            n
          </p>
        </div>
      </div>
      <div className="w-1/2 flex flex-col gap-3 overflow-y-auto h-[600px]">
        <InstructorCard />
        <InstructorCard />
        <InstructorCard />
      </div>
    </div>
  );
}

export default ListInstructors;
