import React from "react";
import ListInstructors from "./ListInstructors";

function TopInstructor() {
  return (
    <div>
      <h1 className="font-bold text-3xl text-blue_3 text-center mb-2">
        Our Top Instructors
      </h1>
      <div className="flex justify-end">
        <a className="text-blue_2 cursor-pointer mb-2">View all</a>
      </div>
      <ListInstructors />
    </div>
  );
}

export default TopInstructor;
