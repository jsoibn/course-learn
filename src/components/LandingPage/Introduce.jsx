import { faCalendar, faUser } from "@fortawesome/free-regular-svg-icons";
import { faCodeBranch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

function Introduce() {
  const { isLogin } = useSelector((state) => state.accountReducer);

  return (
    <div className="w-full relative flex ">
      <div className="w-3/5 flex justify-center self-center   ">
        <div className="pl-20 intro-content "
          data-aos='fade-down-right' data-aos-delay='300' data-aos-offset='300'>
          <h1 className="font-bold text-3xl mb-2">
            <span className="text-blue_2">Programming</span> Courses Here,{" "}
            <br></br>
            Knowledge Without <span className="text-blue_2">Limits</span>
          </h1>
          <p className="text-text_color_base mb-2">
            With us, you will feel more confident and ready to tackle any
            programming challenges
          </p>

          {!isLogin && (
            <button className="bg-blue_2 text-white py-2 px-6 rounded-3xl">
              Join Now
            </button>
          )}
        </div>
      </div>

      <div className="h-4/12 bg-gray_1 w-9/12 flex justify-between absolute bottom-0 rounded-r-[20px] left-0 z-10 p-6"
        data-aos='fade-up'
        data-aos-anchor-placement='bottom-bottom'>
        <div className="w-1/3 p-2 ">
          <div className="mb-2">
            <FontAwesomeIcon
              className="text-yellow-300 text-xl"
              icon={faCalendar}
            />
          </div>
          <h2 className="font-bold mb-2 text-xl">Innovation</h2>
          <p>Stay current with industry trends in our courses</p>
        </div>
        <div className="w-1/3 p-2 ">
          <div className="mb-2">
            <FontAwesomeIcon className="text-blue-400 text-xl" icon={faUser} />
          </div>
          <h2 className="font-bold mb-2 text-xl">Mentor</h2>
          <p>
            Experienced, professional instructors for confident, quality
            learning
          </p>
        </div>
        <div className="w-1/3 p-2 ">
          <div className="mb-2">
            <FontAwesomeIcon
              className="text-red-400 text-xl"
              icon={faCodeBranch}
            />
          </div>
          <h2 className="font-bold mb-2 text-xl">Community</h2>
          <p>
            Supportive environment with online forums, resources, and peer
            learning opportunities
          </p>
        </div>
      </div>

      <div className="w-5/12 h-full" data-aos='fade-right' data-aos-delay='400' data-aos-offset='500'>

        <img
          src="/assests/images/landing_pages/land_page_1.jpg"
          className="w-full h-full"
          alt=""
        />
      </div>
    </div>
  );
}

export default Introduce;
