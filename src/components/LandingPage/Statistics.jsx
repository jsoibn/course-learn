import React from "react";
import { Facts } from "./Features/Fact";
import CountUp from "react-countup";

function Statistics() {
  return (
    <div
      className="px-40 py-12 bg-white bg-contain bg-no-repeat bg-left-top"
      style={{
        backgroundImage: `url(/assests/images/landing_pages/cool-background.png)`,
      }}
    >
      <div className="flex justify-between items-center z-10 bg-gray-100 rounded-md px-32 py-14">
        {Facts &&
          Facts.map((item, index) => {
            const { startNumber, endNumber, title } = item;
            return (
              <div key={index}>
                <h2 className="text-blue_3 text-3xl font-bold">
                  <CountUp
                    start={startNumber}
                    end={endNumber}
                    duration={2}
                    enableScrollSpy
                  />
                  +
                </h2>
                <div className="flex flex-col justify-center items-center">
                  <p className="text-text_color_base">{title}</p>
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
}

export default Statistics;
