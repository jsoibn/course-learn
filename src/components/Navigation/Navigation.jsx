import {
  faBookOpen,
  faHouse,
  faRightFromBracket,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { NavLink, useLocation, useNavigate } from "react-router-dom";
import storageService from "../../api/storageService";
import { useDispatch, useSelector } from "react-redux";
import accountSlice, {
  setIsLogin,
  setRole,
} from "../../store/slices/accountSlice";
import { isCancelForm } from "../../store/slices/courseSlice/createCourseSlice";

function Navigation() {
  const location = useLocation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { role } = useSelector((state) => state.accountReducer);

  const handleLogout = () => {
    storageService.removeAccessToken();
    dispatch(setIsLogin(false));
    dispatch(setRole(""));
    storageService.removeRole();

    navigate("/");
    window.location.reload();
  };
  const handleCancelForm = () => {
    dispatch(isCancelForm());
  };

  return (
    <div className="bg-black h-full flex flex-col">
      <NavLink to="/instructor">
        <img
          className="rounded-full"
          src="/assests/images/Code_IT-removebg-preview.png"
          alt=""
        />
      </NavLink>
      <ul className="flex flex-col flex-grow items-center">
        <li className="py-4 w-full">
          <NavLink
            to="/instructor/profile"
            className={`flex flex-col items-center ${
              location.pathname === "/profile"
                ? " bg-white text-black relative  after:absolute after:right-0 after:bottom-[-20px] after:w-[20px] after:h-[20px] after:rounded-tr-[20px] after:bg-black after:shadow-box_shadow_4 before:absolute before:right-0 before:top-[-20px] before:w-[20px] before:h-[20px] before:bg-black before:rounded-br-[20px] before:shadow-box_shadow_3 "
                : "text-white "
            }`}
          >
            <FontAwesomeIcon className="py-2 text-[20px]" icon={faUser} />
            <p className="text-[16px] z-20">User</p>
          </NavLink>
        </li>
        {role === "Instructor" ? (
          <li className="py-4 w-full">
            <NavLink
              onClick={handleCancelForm}
              to="/instructor/catalog"
              className={`flex flex-col items-center ${
                location.pathname === "/catalog"
                  ? " bg-white text-black relative  after:absolute after:right-0 after:bottom-[-20px] after:w-[20px] after:h-[20px] after:rounded-tr-[20px] after:bg-black after:shadow-box_shadow_4 before:absolute before:right-0 before:top-[-20px] before:w-[20px] before:h-[20px] before:bg-black before:rounded-br-[20px] before:shadow-box_shadow_3 "
                  : "text-white "
              }`}
            >
              <FontAwesomeIcon className="py-2 text-[20px]" icon={faHouse} />
              <p className="text-[16px] z-20">Courses</p>
            </NavLink>
          </li>
        ) : (
          <>
            <li className="py-4 w-full">
              <NavLink
                exact
                to="/instructor/mycourses"
                className={({ isActive }) =>
                  isActive
                    ? " text-black flex flex-col items-center bg-white relative  after:absolute after:right-0 after:bottom-[-20px] after:w-[20px] after:h-[20px] after:rounded-tr-[20px] after:bg-black after:shadow-box_shadow_4 before:absolute before:right-0 before:top-[-20px] before:w-[20px] before:h-[20px] before:bg-black before:rounded-br-[20px] before:shadow-box_shadow_3  "
                    : "text-white flex flex-col items-center"
                }
              >
                <FontAwesomeIcon
                  className="py-2 text-[20px]"
                  icon={faBookOpen}
                />
                <p className="text-[16px] z-20">Courses</p>
              </NavLink>
            </li>
            <li className="py-4 w-full">
              <NavLink
                exact
                to="/instructor/subcription"
                className={({ isActive }) =>
                  isActive
                    ? " text-black  flex flex-col items-center bg-white relative  after:absolute after:right-0 after:bottom-[-20px] after:w-[20px] after:h-[20px] after:rounded-tr-[20px] after:bg-black after:shadow-box_shadow_4 before:absolute before:right-0 before:top-[-20px] before:w-[20px] before:h-[20px] before:bg-black before:rounded-br-[20px] before:shadow-box_shadow_3  "
                    : "text-white flex flex-col items-center"
                }
              >
                <FontAwesomeIcon className="py-2 text-[20px]" icon={faUser} />
                <p className="text-[16px] z-20">Subcription</p>
              </NavLink>
            </li>
          </>
        )}
      </ul>
      <div className="py-4 w-full flex flex-col justify-center items-center ">
        <button onClick={handleLogout} className="text-white">
          <FontAwesomeIcon
            className="py-2 text-[20px]"
            icon={faRightFromBracket}
          />
          <p className="text-[16px]">Log Out</p>
        </button>
      </div>
    </div>
  );
}

export default Navigation;
