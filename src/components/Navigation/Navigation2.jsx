import React, { useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCartShopping,
  faMagnifyingGlass,
} from "@fortawesome/free-solid-svg-icons";
import ProfileDropdown from "../Header/ProfileDropdown/ProfileDropdown";
import { Link, NavLink, useLocation } from "react-router-dom";
import CatalogNav from "./CatalogNav/CatalogNav";
import { Badge } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getCart } from "../../store/slices/cartSlice";

export default function Navigation2() {
  const { totalQuantity, coursesInOrder } = useSelector(
    (state) => state.cartReducer
  );

  const dispatch = useDispatch();
  const location = useLocation();

  useEffect(() => {
    dispatch(getCart());
    // console.log(coursesInOrder);
  }, []);

  return (
    <>
      <div className="flex py-2  items-center justify-between  border-b border-solid border-gray_2 px-10">
        <Link to="/" className="block w-24">
          <img
            src="/assests/images/Code_IT-removebg-preview.png"
            alt="logo"
            className="w-full"
          />
        </Link>

        <div className="relative w-[500px]">
          <input
            type="text"
            placeholder="Search...."
            className=" w-full px-12 py-1 rounded-[20px] bg-slate-100 placeholder-slate-400"
          />
          <FontAwesomeIcon
            className="absolute z-50 top-[50%] left-2 translate-y-[-50%]"
            icon={faMagnifyingGlass}
            style={{ color: "#969696" }}
          />
        </div>

        <div className="flex items-center gap-8">
          <ul className=" flex gap-7 text-[17px] font-normal    ">
            <li>
              <NavLink
                to="/studyingcourse"
                className={` ${
                  location.pathname === "/studyingcourse"
                    ? " bg-blue_5 p-2 text-white rounded-[15px] relative after:absolute after:bottom-[-7px] after:left-[50%] after:translate-x-[-50%] after:transform after:rotate-45  after:w-[15px] after:h-[15px] after:bg-blue_5 "
                    : "p-2 text-black "
                }`}
              >
                Studying Progress
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/subcription"
                className={` ${
                  location.pathname === "/subcription"
                    ? " bg-blue_5 p-2 text-white rounded-[15px] relative after:absolute after:bottom-[-7px] after:left-[50%] after:translate-x-[-50%] after:transform after:rotate-45  after:w-[15px] after:h-[15px] after:bg-blue_5 "
                    : "p-2 text-black "
                }`}
              >
                Subcription
              </NavLink>
            </li>
          </ul>

          <div className="flex items-center gap-5">
            <Link to="/cart" className="block relative">
              <FontAwesomeIcon
                icon={faCartShopping}
                className="text-[20px] block "
              />
              <p className="text-[10px] w-4 h-4 flex justify-center items-center bg-red-500 text-white rounded-full p-[2px] absolute -top-2 -right-2">
                {totalQuantity}
              </p>
            </Link>
            <ProfileDropdown />
          </div>
        </div>
      </div>
      <CatalogNav />
      {/* {location.pathname === "/" ? <CatalogNav /> : ""} */}
    </>
  );
}
