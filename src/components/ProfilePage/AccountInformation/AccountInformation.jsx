import React, { useEffect } from "react";
import yup from "../../../YupGlobal/YupGlobal";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useDispatch, useSelector } from "react-redux";
import { Form, Input, Button, Select, Space, Image } from "antd";
import { isUpdating } from "../../../store/slices/instructorSlice";
import { fetchUserProfile, updateUserProfile, } from "../../../store/slices/userSlice";

const { Option } = Select;

function AccountInformation() {
  const { isUpdate } = useSelector((state) => state.instructorReducer);
  const dispatch = useDispatch();
  const { profile } = useSelector((state) => state.userReducer);
  const [form] = Form.useForm();
  const schema = yup.object().shape({
    gender: yup.string().required("Please select a gender"),
    phoneNumber: yup.string().phoneNumber().required("Phone number is required"),
    image: yup.string().url("Invalid URL format").required("Image URL is required"),

  });


  const yupSync = {
    async validator({ field }, value) {
      await schema.validateSyncAt(field, { [field]: value });
    },
  };


  const handleSave = (values) => {
    dispatch(isUpdating(false));
    dispatch(updateUserProfile(values))
      .then(() => {
        toast.success("Update successed!");
      })
      .catch((error) => {
        console.error("Error:", error);
      });
    dispatch(fetchUserProfile(values));
  };

  const handleCancel = () => {
    form.setFieldsValue(profile);
    dispatch(isUpdating(false));
  };

  useEffect(() => {
    dispatch(fetchUserProfile());
  }, [dispatch, isUpdate]);
  console.log("User profile", profile);

  return (
    <div>
      <ToastContainer />
      <Form
        form={form}
        onFinish={handleSave}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        layout="horizontal"
        initialValues={profile}
        validationSchema={schema}
      >
        <Form.Item label="Image" name="image" rules={[yupSync]}>
          {isUpdate ? (
            <>
              <Space>
                <Input
                  placeholder="Image URL"
                  style={{ width: 200 }}
                  allowClear
                />
              </Space>
            </>
          ) : (
            <Image src={profile?.image} alt="User Image" width={100} />
          )}
        </Form.Item>

        <Form.Item
          label="First Name"
          name="firstName"
          rules={[
            {
              required: true,
              message: "Please enter your first name",
            },
          ]}
        >
          {isUpdate ? (
            <Input
              placeholder="First name"
              style={{ width: 200 }}
              disabled={!isUpdate}
            />
          ) : (
            <span>{profile?.firstName}</span>
          )}
        </Form.Item>


        <Form.Item
          label="Last Name"
          name="lastName"
          rules={[
            {
              required: true,
              message: "Please enter your last name",
            },
          ]}
        >
          {isUpdate ? (
            <Input
              placeholder="Last name" style={{ width: 200 }} disabled={!isUpdate} />
          ) : (
            <span>{profile?.lastName}</span>
          )}
        </Form.Item>

        <Form.Item label="Email" name="email">
          <span>{profile?.email}</span>
        </Form.Item>

        <Form.Item
          label="Phone Number"
          name="phoneNumber"
          rules={[yupSync]}
        >
          {isUpdate ? (
            <Input
              placeholder="Phone Number"
              style={{ width: 200 }}
              disabled={!isUpdate}
            />
          ) : (
            <span>{profile?.phoneNumber}</span>
          )}
        </Form.Item>

        <Form.Item
          label="Gender"
          name="gender"
          rules={[yupSync]}
        >
          {isUpdate ? (
            <Select style={{ width: 200 }} disabled={!isUpdate}>
              <Option value="0">Male</Option>
              <Option value="1">Female</Option>
              <Option value="2">Other</Option>
            </Select>
          ) : (
            <span>{profile?.gender}</span>
          )}
        </Form.Item>

        <Form.Item label="Address" name="address" rules={[
          {
            required: true,
            message: "Please enter your address",
          },
        ]}
        >
          {isUpdate ? (
            <Input placeholder="Address" style={{ width: 200 }} disabled={!isUpdate} />
          ) : (
            <span>{profile?.address}</span>
          )}
        </Form.Item>

        {isUpdate && (
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Space>
              <Button className="!bg-blue-300" htmlType="submit">
                Save
              </Button>
              <Button type="default" onClick={handleCancel}>
                Cancel
              </Button>
            </Space>
          </Form.Item>
        )}
      </Form>
    </div>
  );
}

export default AccountInformation;