import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchUserProfile } from "../../../store/slices/userSlice";

function ProfileDetail() {
  const dispatch = useDispatch();
  const { profile } = useSelector((state) => state.userReducer);
  useEffect(() => {
    dispatch(fetchUserProfile());
  }, [dispatch]);

  if (!profile) {
    return null;
  }
  return (
    <>
      <div className="w-full h-[60%] bg-white py-5 px-5 flex flex-col items-center rounded-xl">
        <div className="w-[50%] h-[50%]">
          <img
            className="rounded-full w-full h-full"
            src={profile.image}
            alt="Avatar"
          />
        </div>
        <h1 className="font-bold text-xl mt-3">
          {profile.lastName + " " + profile.firstName}
        </h1>
      </div>

      <div className="w-full h-[50%] bg-white py-5 px-5 rounded-xl">
        <div>
          <p className="text-blue-500">Email</p>
          <p>{profile.email}</p>
        </div>
        <div className="mt-5">
          <p className="text-blue-500">Phone</p>
          <p>{profile.phoneNumber}</p>
        </div>
        <div className="mt-5">
          <p className="text-blue-500">Address</p>
          <p>{profile.address}</p>
        </div>
      </div>
    </>
  );
}

export default ProfileDetail;
