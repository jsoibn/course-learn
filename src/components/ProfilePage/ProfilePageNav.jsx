import React from "react";
import { useSelector } from "react-redux";

function ProfilePageNav() {
  const { profile } = useSelector((state) => state.userReducer);

  return (
    <div className="w-full px-10 ">
      <div className="w-full h-[80px] flex items-center justify-between border-b border-solid border-gray_2">
        <div>
          <h1 className="font-bold text-3xl">Profile</h1>
        </div>
        <div className="w-12 h-12 bg-black rounded-full relative cursor-pointer">
          <img
            className="rounded-full w-full h-full"
            src={profile?.image}
            alt="Avatar"
          />
        </div>
      </div>
    </div>
  );
}

export default ProfilePageNav;
