import React from "react";
import Navigation from "../components/Navigation/Navigation";
import { Navigate, Outlet } from "react-router-dom";

const MainLayout = ({ isAllowed, redirectPath = "/", children }) => {
  if (!isAllowed) {
    console.log(isAllowed);
    return <Navigate to={redirectPath} replace />;
  }

  return children ? (
    children
  ) : (
    <div className="w-full h-[100vh] relative flex bg-black py-2 pr-4">
      <div className="w-[9%] h-[99%] sticky top-0 left-0 ">
        <Navigation />
      </div>
      <div className="w-full h-[99%]  right-0 top-0 bg-white rounded-2xl ">
        <Outlet />
      </div>
    </div>
  );
};

export default MainLayout;
