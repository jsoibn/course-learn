import React, { useEffect, useState } from "react";
import CardDetail from "../components/CourseDetail/CardDetail";
import ContentDetail from "../components/CourseDetail/ContentDetail";
import TopCourse from "../components/LandingPage/Features/TopCourse/TopCourse";
import HeaderCourseDetail from "../components/CourseDetail/HeaderCourseDetail";
import NavDetail from "../components/CourseDetail/NavDetail";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

export default function CourseDetailPage() {
  const { id } = useParams();
  const { courses } = useSelector((state) => state.courseReducer);
  const [courseCurrent, setCourseCurrent] = useState();

  useEffect(() => {
    window.scrollTo(0, 0);
    const foundCourse = courses?.find((course) => course.id === id);
    if (foundCourse) {
      setCourseCurrent(foundCourse);
    }
  }, [id, courses]);

  return (
    <>
      {/* <NavDetail /> */}
      <div className=" grid grid-cols-10 ">
        <HeaderCourseDetail course={courseCurrent} />

        <div className="col-span-6 pl-[120px] pr-[50px]">
          <ContentDetail course={courseCurrent} />
        </div>
        <div className="col-span-4  pr-[120px] pl-[150px]">
          <CardDetail course={courseCurrent} />
        </div>
      </div>
      <div className="mt-32 ml-16 w-[90%]">
        <TopCourse />
      </div>
    </>
  );
}
