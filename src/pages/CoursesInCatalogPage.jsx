import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
  getCoursesNew,
  getCoursesRate,
} from "../store/slices/courseSlice/courseSlice";
import CatalogNav from "../components/Navigation/CatalogNav/CatalogNav";
import ReusableTitle from "../components/CourseCatalog/CoursesInCatalogPage/ReusableTitle";
import Carousel from "react-multi-carousel";
import CoursesSuggest from "../components/CourseCatalog/CoursesInCatalogPage/CoursesSuggest";

export default function CoursesInCatalogPage() {
  const dispatch = useDispatch();
  const { catlName } = useParams();
  const { catalog } = useSelector((state) => state.courseReducer);
  const decodeCatlName = catlName.replace(/-/g, " ").trim();
  const catalogPage = catalog?.find((item) => item.name === decodeCatlName);
 useEffect(() => {
    dispatch(getCoursesNew({ catalogIDs: catalogPage?.id }));
    dispatch(getCoursesRate({ catalogIDs: catalogPage?.id }));
  }, [dispatch]);

  return (
    <div className="laptop:px-[5%] desktop:px-[10%] py-[40px]">
      <h1 className="text-[28px] font-semibold pb-[30px]">
        {catalogPage?.name}
      </h1>
      <ReusableTitle text="Courses to get you started" />
        <CoursesSuggest/>
    </div>
  );
}
