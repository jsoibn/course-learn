import React from "react";
import CoursesPageNav from "../components/HomeCoursesPage/CoursesPageNav";

import DetailCoursesMain from "../components/HomeCoursesPage/DetailCourses/DetailCoursesMain";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList } from "@fortawesome/free-solid-svg-icons";
import ListCoursesCard from "../components/HomeCoursesPage/ListCourses/ListCoursesCard";

function HomeCoursesPage() {
  return (
    <div className="h-full px-8 pb-5">
      {/* <div className="px-2 h-[10%]">
        <CoursesPageNav />
      </div> */}

      <div className=" grid grid-cols-10 gap-4 h-[90%]  py-5 ">
        <div className=" col-span-4 h-full overflow-y-auto ">
          <div className=" flex items-center justify-between  pl-8 pr-2">
            <div className="flex text-[18px] ">
              <p>All</p>
              <p className="px-8">Active</p>
              <p>Completed</p>
            </div>
            <FontAwesomeIcon className="text-[18px]" icon={faList} />
          </div>
          <div className=" px-2   ">
            <ListCoursesCard />
            <ListCoursesCard />
            <ListCoursesCard />
            <ListCoursesCard />
            <ListCoursesCard />
            <ListCoursesCard />

            <ListCoursesCard />
            <ListCoursesCard />
          </div>
        </div>
        <div className=" col-span-6 bg-gray_5 h-full rounded-[15px] p-6 overflow-y-auto">
          <DetailCoursesMain />
        </div>
      </div>
    </div>
  );
}

export default HomeCoursesPage;
