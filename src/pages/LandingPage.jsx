import React from "react";
import Aos from 'aos';
import 'aos/dist/aos.css';
import Introduce from "../components/LandingPage/Introduce";
import Statistics from "../components/LandingPage/Statistics";
import Features from "../components/LandingPage/Features/Features";
import { motion, useScroll } from "framer-motion";

function LandingPage() {
  Aos.init({
    duration: 1800,
    offset: 100,
  })
  const { scrollYProgress } = useScroll();
  return (
    <>
      <motion.div
        className="fixed top-0 left-0 right-0 h-[5px] transform origin-[0%] bg-blue-600 z-50 "
        style={{ scaleX: scrollYProgress }}
      />
      <div className="relative ">
        {/* <Header /> */}
        <Introduce />
        <Statistics />
        <Features />
      </div>
    </>
  );
}

export default LandingPage;
