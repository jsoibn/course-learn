import React, { useState } from "react";
import CoursesPageNav from "../components/HomeCoursesPage/CoursesPageNav";
import SectionCourse from "../components/HomeCoursesPage/LearnCourses/SectionCourse";
import NoteCourse from "../components/HomeCoursesPage/LearnCourses/NoteCourse";
import VideoCourse from "../components/HomeCoursesPage/LearnCourses/VideoCourse";

export default function LearnCoursePage(){
    const [sectionName, setSectionName] = useState(null);
    return(
        <div className="h-full px-8">
             <div className="px-2">  <CoursesPageNav /></div>
         <div className=" w-full px-2 pt-4 flex">
            <div className="w-[25%] h-full">
            <SectionCourse setSectionName={setSectionName} />
            </div>
            <div className="w-[50%] px-6 h-full">
            <VideoCourse sectionName={sectionName} />
            </div>
            <div className="w-[25%] h-full ">
                <NoteCourse/>
            
            </div>
      </div>
        </div>
    )
}