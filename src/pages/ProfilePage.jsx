import React, { useEffect, useState } from "react";
import ProfilePageNav from "../components/ProfilePage/ProfilePageNav";
import ProfileDetail from "../components/ProfilePage/ProfileDetail/ProfileDetail";
import ChangePassword from "../components/ProfilePage/ModalChangePassword/ChangePassword";
import AccountInformation from "../components/ProfilePage/AccountInformation/AccountInformation";
import { useDispatch, useSelector } from "react-redux";
import { isUpdating, isChangePass } from "../store/slices/instructorSlice";

function ProfilePage() {
  const { isUpdate, isChanged } = useSelector(
    (state) => state.instructorReducer
  );
  const { role } = useSelector((state) => state.accountReducer);
  const dispatch = useDispatch();
  const [showInfo, setShowInfo] = useState(true);
  const handleUpdate = () => {
    dispatch(isUpdating(true));
    dispatch(isChangePass(false));
  };

  const handleChangePass = () => {
    dispatch(isChangePass(true));
    dispatch(isUpdating(false));
  };

  useEffect(() => {}, [isUpdate, isChanged]);
  return (
    <div className="h-full">
      {role === "Instructor" && (
        <div className="h-[10%]">
          <ProfilePageNav />
        </div>
      )}

      <div className="w-full h-[90%] px-9 py-4">
        <div className="bg-gray_1 w-full h-full px-10 pt-5 flex rounded-2xl">
          <div className="w-[30%] h-full flex flex-col justify-between py-5 px-5 gap-2">
            <ProfileDetail />
          </div>
          <div className="w-[70%] h-full py-5 pr-5">
            <div className="flex justify-between h-[10%] items-center">
              {isChanged === false && (
                <h1 className="font-bold text-xl">Account Information</h1>
              )}
              {isChanged && (
                <h1 className="font-bold text-xl">Change Password</h1>
              )}
              <div className="flex">
                <button
                  onClick={handleChangePass}
                  type="button"
                  className="!bg-green-600 text-white px-4 py-2 rounded-2xl hover:!bg-black mr-2 "
                >
                  Change Password
                </button>
                <button
                  onClick={handleUpdate}
                  type="button"
                  className="!bg-green-600 text-white px-4 py-2 rounded-2xl hover:!bg-black "
                >
                  Change Information
                </button>
              </div>
            </div>
            <div className="mt-3 h-[80%]">
              {isChanged === true && <ChangePassword />}
              {showInfo && isChanged === false && <AccountInformation />}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProfilePage;
