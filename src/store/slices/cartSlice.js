import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import cartApi from "../../api/cartApi";

export const getCart = createAsyncThunk("cart", async () => {
  try {
    const res = await cartApi.getCart();
    return res.data._data;
  } catch (error) {
    throw error;
  }
});

export const addToCartS = createAsyncThunk("cart/add-to-cart", async (data) => {
  console.log(data);
  const { id } = data;
  const addToData = {
    courseID: id,
  };

  try {
    const res = await cartApi.addToCart(addToData);
    return data;
  } catch (error) {
    throw error;
  }
});

export const deleteCartItem = createAsyncThunk(
  "cart/delete-cart-item",
  async (data) => {
    const id = data;
    const deleteItem = {
      courseID: id,
    };

    try {
      const res = await cartApi.removeItem(deleteItem);
      return res;
    } catch (error) {
      throw error;
    }
  }
);

const initialState = {
  error: "",
  totalQuantity: 0,
  coursesInOrder: [],
  isLoading: false,
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getCart.fulfilled, (state, action) => {
      return {
        ...state,
        coursesInOrder: action.payload.items,
        totalQuantity: [action.payload.items.length],
      };
    });
    builder.addCase(addToCartS.pending, (state, action) => {
      return {
        ...state,
        isLoading: true,
      };
    });

    builder.addCase(addToCartS.fulfilled, (state, action) => {
      const { coursesInOrder } = state;
      const newCart = [...coursesInOrder, action.payload];
      return {
        ...state,
        coursesInOrder: newCart,
        totalQuantity: newCart.length,
        isLoading: false,
      };
    });

    builder.addCase(deleteCartItem.fulfilled, (state, action) => {
      const { coursesInOrder } = state;
      const idDelete = action.payload.data._data.courseID;
      const newCart = coursesInOrder.filter(
        (item) => item.courseID !== idDelete
      );
      return {
        ...state,
        coursesInOrder: newCart,
        totalQuantity: newCart.length,
      };
    });
  },
});

export const {} = cartSlice.actions;
export default cartSlice.reducer;
