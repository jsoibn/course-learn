import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import courseApi from "../../../api/courseApi";
const initialState = {
  catalog: null,
  error: "",
  isLoading: false,
  courses: null,
  listCoursesNew: null,
  listCoursesRate: null,
  listCoursesPopular: null,
  currentPage: 1,
  perPage: 12,
  courseSort: 0,
  page: 0,
  currentStep: 0,
};

export const getCatalog = createAsyncThunk("course/get_catalog", async () => {
  try {
    const res = await courseApi.getCatalog();
    return res.data._data;
  } catch (error) {
    console.log(error);
  }
});

// Course
export const getCourses = createAsyncThunk(
  "course/get_top_course",
  async ({ limit, catalogIDs, courseSort }) => {
    const CourseParams = {
      offset: 0,
      limit: 999,
      minPrice: 0,
      maxPrice: 9999,
      catalogIDs: catalogIDs,
      courseSort: courseSort,
    };
    try {
      const res = await courseApi.getCourses(CourseParams);
      return res.data._data.list;
    } catch (error) {
      throw error;
    }
  }
);
export const getCoursesNew = createAsyncThunk(
  "course/get_Courses_New",
  async ({ catalogIDs }) => {
    const CourseParams = {
      offset: 0,
      limit: 10,
      minPrice: 0,
      maxPrice: 9999,
      catalogIDs: catalogIDs,
      courseSort: 3,
    };
    try {
      const res = await courseApi.getCourses(CourseParams);
      return res.data._data.list;
    } catch (error) {
      throw error;
    }
  }
);
export const getCoursesRate = createAsyncThunk(
  "course/get_Courses_Rate",
  async ({ catalogIDs }) => {
    const CourseParams = {
      offset: 0,
      limit: 10,
      minPrice: 0,
      maxPrice: 9999,
      catalogIDs: catalogIDs,
      courseSort: 0,
    };
    try {
      const res = await courseApi.getCourses(CourseParams);
      return res.data._data.list;
    } catch (error) {
      throw error;
    }
  }
);
//Pagination
export const fetchCoursesByPage = createAsyncThunk(
  "course/fetchCoursesByPage",
  async (page, { getState }) => {
    const state = getState();
    const perPage = state.courseReducer.perPage;
    const offset = (page - 1) * perPage;

    try {
      const res = await courseApi.getCourses({
        offset,
        limit: 999,
        minPrice: 0,
        maxPrice: 9999,
        catalogIDs: state.courseReducer.catalogIDs,
        courseSort: state.courseReducer.courseSort,
      });
      return res.data._data.list;
    } catch (error) {
      throw error;
    }
  }
);

export const getCoursesIns = createAsyncThunk(
  "course/get-by-instructor",
  async ({ limit }) => {
    const CourseInsParams = {
      offset: 0,
      limit: limit,
    };
    try {
      const res = await courseApi.getCoursesByIns(CourseInsParams);
      return res.data._data.list;
    } catch (error) {
      throw error;
    }
  }
);

const courseSlice = createSlice({
  name: "course",
  initialState,
  reducers: {
    setCurrentPage: (state, action) => {
      state.currentPage = action.payload;
    },
    setPage: (state, action) => {
      state.page = action.payload;
    },
    increDecrePage: (state, action) => {
      const { currentPage } = state;
      return {
        ...state,
        currentPage: currentPage + action.payload,
      };
    },
    setFilter: (state, action) => {
      state.courseSort = action.payload;
    },
    changeStep: (state, action) => {
      if (action.payload !== 0) {
        const ans = state.currentStep + action.payload;
        return {
          ...state,
          currentStep: ans,
        };
      } else {
        return {
          ...state,
          currentStep: 0,
        };
      }
    },
  },
  extraReducers: (builder) => {
    //getCatalog
    builder.addCase(getCatalog.fulfilled, (state, action) => {
      return { ...state, catalog: action.payload, error: "" };
    });
    builder.addCase(getCatalog.rejected, (state, action) => {
      return { ...state, error: action.payload };
    });
    builder.addCase(getCourses.fulfilled, (state, action) => {
      return { ...state, courses: action.payload, error: "", isLoading: false };
    });
    builder.addCase(getCourses.rejected, (state, action) => {
      return { ...state, error: action.payload, isLoading: false };
    });
    builder.addCase(getCourses.pending, (state, action) => {
      return { ...state, isLoading: true };
    });
    builder.addCase(getCoursesNew.fulfilled, (state, action) => {
      return { ...state, listCoursesNew: action.payload, error: "" };
    });
    builder.addCase(getCoursesNew.rejected, (state, action) => {
      return { ...state, listCoursesNew: action.payload };
    });
    builder.addCase(getCoursesRate.fulfilled, (state, action) => {
      return { ...state, listCoursesRate: action.payload, error: "" };
    });
    builder.addCase(getCoursesRate.rejected, (state, action) => {
      return { ...state, listCoursesRate: action.payload };
    });
    builder.addCase(fetchCoursesByPage.fulfilled, (state, action) => {
      return { ...state, courses: action.payload, error: "" };
    });
    builder.addCase(fetchCoursesByPage.rejected, (state, action) => {
      return { ...state, error: action.payload };
    });
    builder.addCase(getCoursesIns.fulfilled, (state, action) => {
      const sortCourse = [...action.payload];
      sortCourse.sort((a, b) => b.createdDate.localeCompare(a.createdDate));
      return {
        ...state,
        courses: sortCourse,
        error: "",
      };
    });
    builder.addCase(getCoursesIns.rejected, (state, action) => {
      return { ...state, error: action.payload, isLoadingIns: false };
    });
  },
});

export const {
  toggleFilterSideBar,
  setCurrentPage,
  setPage,
  increDecrePage,
  setFilter,
  changeStep,
} = courseSlice.actions;

export default courseSlice.reducer;
