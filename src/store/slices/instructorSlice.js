import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isUpdate: false,
  isChanged: false,
  isField1Filled: false,
};

const instructorSlice = createSlice({
  name: "instructor",
  initialState,
  reducers: {
    isUpdating: (state, action) => {
      return {
        ...state,
        isUpdate: action.payload,
      };
    },
    isChangePass: (state, action) => {
      return {
        ...state,
        isChanged: action.payload,
      };
    },
    isField1Filled: (state,action) => {
     
      return {
        ...state,
        isField1Filled : action.payload
      }
    }
  },
});

export const { isUpdating, isChangePass,isField1Filled } = instructorSlice.actions;

export default instructorSlice.reducer;
