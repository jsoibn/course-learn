import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import userApi from "../../api/userApi";

export const fetchUserProfile = createAsyncThunk(
  "user/get_user_profile",
  async () => {
    try {
      const response = await userApi.get_user_profile();
      return response.data._data;
    } catch (error) {
      console.log(error);
    }
  }
);

export const updateUserProfile = createAsyncThunk(
  "user/update_user",
  async (userProfile) => {
    try {
      const response = await userApi.update_user(userProfile);
      return response.data;
    } catch (error) {
      throw error;
    }
  }
);

const userSlice = createSlice({
  name: "user",
  initialState: {
    profile: null,
    loading: "idle",
    error: null,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchUserProfile.pending, (state) => {
        state.loading = "loading";
      })
      .addCase(fetchUserProfile.fulfilled, (state, action) => {
        state.loading = "succeeded";
        state.profile = action.payload;
      })
      .addCase(fetchUserProfile.rejected, (state, action) => {
        state.loading = "failed";
        state.error = action.error.message;
      })
      .addCase(updateUserProfile.fulfilled, (state, action) => {
        state.loading = "succeeded";
        state.profile = action.payload;
      })
      .addCase(updateUserProfile.rejected, (state, action) => {
        state.loading = "failed";
        state.error = action.error.message;
      });
  },
});
export const {} = userSlice.actions;

export default userSlice.reducer;
